package main

import (
	"errors"
	"strconv"

	"gitee.com/bon-ami/eztools/v3"
	_ "github.com/go-sql-driver/mysql"
)

func qryPrj(db *eztools.Dbs, product string) {
	var (
		selStr []string
		info   string
	)
	selStr = make([]string, 1)
	selStr[0] = fldGoogle
	res, err := db.Search(tblProdgle, fldProduct+"="+product, selStr, "")
	if err != nil {
		return
	}
	if len(res) < 1 {
		eztools.ShowStrln("NO results found.")
		return
	}
	ch := make(chan string)
	j := 0
	for _, i := range res {
		go listReq(db, fldId+"="+i[0], ch)
		j++
	}
	for {
		info = <-ch
		if len(info) > 0 {
			eztools.ShowStrln(info)
		} else {
			j--
			if j == 0 {
				break
			}
		}
	}
}

// cri: criteria when modifying, nil if adding only. [0]=bit, [1]=android, [2]=phase
func addOrModProdFo(db *eztools.Dbs, product string, cri *[]string) (err error) {
	mb, err := choosePairOrAdd(db, true, true,
		tblBit, tblAndroid, tblPhase)
	if err != nil {
		return
	}
	if len(mb[tblBit]) < 1 ||
		len(mb[tblAndroid]) < 1 ||
		len(mb[tblPhase]) < 1 {
		return errors.New("empty value(s)")
	}
	fields := []string{fldBit, fldProduct, fldAndroid, fldPhase}
	values := []string{mb[tblBit], product,
		mb[tblAndroid], mb[tblPhase]}
	if cri == nil {
		_, err = db.AddWtParams(tblProdfo, fields, values, false)
	} else {
		if len(*cri) < 3 {
			return errors.New("Not enough criteria!")
		}
		criStr := fldProduct + "=\"" + product + "\""
		for i := 0; i < 3; i++ {
			if len((*cri)[i]) > 0 {
				criStr += " AND "
				switch i {
				case 0:
					criStr += fldBit
				case 1:
					criStr += fldAndroid
				case 2:
					criStr += fldPhase
				}
				criStr += "=\"" + (*cri)[i] + "\""
			}
		}
		err = db.UpdateWtParams(tblProdfo,
			criStr, fields, values, false, false)
	}
	if err != nil {
		return
	}
	return
}

func upPrj(db *eztools.Dbs, readonly bool) {
	mp, err := choosePairOrAdd(db, !readonly, false, tblProduct)
	if err != nil {
		return
	}
	qryPrj(db, mp[fldProduct])
	if readonly {
		return
	}
	selStr := []string{fldBit, fldAndroid, fldPhase}
	searched, err := db.Search(tblProdfo,
		fldProduct+"="+mp[fldProduct],
		selStr, "")
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}
	if len(searched) < 1 {
		if err := addOrModProdFo(db, mp[fldProduct], nil); err != nil {
			eztools.LogErrPrint(err)
			return
		}
	} else {
		for i := 0; i < len(searched); i++ {
			eztools.ShowStrln(strconv.Itoa(i) + ": bit " +
				getStrFromID(db, tblBit, searched[i][0]) +
				", android " +
				getStrFromID(db, tblAndroid, searched[i][1]) +
				", phase " +
				getStrFromID(db, tblPhase, searched[i][2]))
		}
	}
	answer := eztools.PromptStr("Input a number of above to change an item. Input \"a\" to add one item. Input nothing to skip product maintenance.")
	switch answer {
	case "a":
		err = addOrModProdFo(db, mp[fldProduct], nil)
	case "":
	default:
		var ans int
		ans, err = strconv.Atoi(answer)
		if err == nil && ans >= 0 && ans < len(searched) {
			err = addOrModProdFo(db, mp[fldProduct], &searched[ans])
		}
	}
	if err != nil {
		eztools.LogErrPrint(err)
	}
	mo, err := choosePairOrAdd(db, false, false, tblTool, tblAndroid, tblVer)
	if err != nil {
		return
	}
	err = addOrUpdateProdgle(db, mp[fldProduct], mo[tblTool], mo[tblVer], mo[tblAndroid])
	if err != nil {
		eztools.LogErrPrint(err)
	}
}
