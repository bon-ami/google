package main

import (
	"flag"
	"os"
	"strconv"
	"time"

	"gitee.com/bon-ami/eztools/v3"
	_ "github.com/go-sql-driver/mysql"
)

const (
	tblGOOGLE  = "TblGoogle"
	tblTOOL    = "TblTool"
	tblANDROID = "TblAndroid"
	tblVER     = "TblVer"
	tblPRODGLE = "TblProdgle"
	tblPRODFO  = "TblProdfo"
	tblPRODUCT = "TblProduct"
	tblBIT     = "TblBit"
	tblPHASE   = "TblPhase"
	fldTOOL    = "FldTool"
	fldANDROID = "FldAndroid"
	fldGOOGLE  = "FldGOOGLE"
	fldVER     = "FldVer"
	fldREQ     = "FldReq"
	fldEXP     = "FldExp"
	fldBIT     = "FldBit"
	fldPRODUCT = "FldProduct"
	fldPHASE   = "FldPhase"
)

var (
	ver    string
	server string
	fldId, fldStr, fldBit, fldGoogle, fldProduct, fldPhase, fldTool, fldAndroid, fldVer, fldReq, fldExp,
	tblPhase, tblBit, tblTool, tblAndroid, tblVer, tblProdgle, tblProdfo, tblProduct, tblGoogle, tblDef string
)

func initVarsFromDb(db *eztools.Dbs) {
	tblDef = db.GetTblDef()
	fldId = db.GetFldID()
	fldStr = db.GetFldStr()
	tblGoogle, _ = db.GetPair(tblDef, tblGOOGLE, fldId, fldStr)
	fldBit, _ = db.GetPair(tblDef, fldBIT, fldId, fldStr)
	fldProduct, _ = db.GetPair(tblDef, fldPRODUCT, fldId, fldStr)
	fldPhase, _ = db.GetPair(tblDef, fldPHASE, fldId, fldStr)
	fldTool, _ = db.GetPair(tblDef, fldTOOL, fldId, fldStr)
	fldAndroid, _ = db.GetPair(tblDef, fldANDROID, fldId, fldStr)
	fldVer, _ = db.GetPair(tblDef, fldVER, fldId, fldStr)
	fldReq, _ = db.GetPair(tblDef, fldREQ, fldId, fldStr)
	fldExp, _ = db.GetPair(tblDef, fldEXP, fldId, fldStr)
	fldGoogle, _ = db.GetPair(tblDef, fldGOOGLE, fldId, fldStr)
	tblTool, _ = db.GetPair(tblDef, tblTOOL, fldId, fldStr)
	tblAndroid, _ = db.GetPair(tblDef, tblANDROID, fldId, fldStr)
	tblVer, _ = db.GetPair(tblDef, tblVER, fldId, fldStr)
	tblProdgle, _ = db.GetPair(tblDef, tblPRODGLE, fldId, fldStr)
	tblProdfo, _ = db.GetPair(tblDef, tblPRODFO, fldId, fldStr)
	tblProduct, _ = db.GetPair(tblDef, tblPRODUCT, fldId, fldStr)
	tblBit, _ = db.GetPair(tblDef, tblBIT, fldId, fldStr)
	tblPhase, _ = db.GetPair(tblDef, tblPHASE, fldId, fldStr)
}

func main() {
	var (
		err            error
		db             *eztools.Dbs
		paramH, paramD bool
		paramL         string
	)
	var readonly bool
	if ver != "dev" {
		readonly = true
	}
	if !readonly {
		paramD = true
	}
	paramL = "google.log"
	flag.BoolVar(&paramH, "h", false, "Print info message")
	flag.BoolVar(&paramD, "d", paramD, "Print debug messages")
	flag.StringVar(&paramL, "l", paramL, "to specify the name of Log file")
	flag.Parse()
	if paramH {
		eztools.ShowStrln("V0.1 current requirements on the bottom when shown.")
		eztools.ShowStrln("V1.0 auto upgrade.")
		eztools.ShowStrln("V1.1 adb messages hidden.")
		eztools.ShowStrln("V2.0 Resource request supported for PAB. Most empty answers return by default.")
		eztools.ShowStrln("V2.1 Resource request supported for STS.")
		eztools.ShowStrln("V2.2 Resource request supported for CTS, GTS.")
		eztools.ShowStrln("V2.3 Resource request supported for any files. Transfer progress supported.")
		//eztools.ShowStrln("V1.1 Open Source")
		flag.Usage()
		return
	}
	if paramD {
		eztools.Debugging = true
	}

	_, week := time.Now().ISOWeek()
	eztools.ShowStrln("V" + ver + ". Now it is week " + strconv.Itoa(week))
	db, _, err = eztools.MakeDbs()
	if err != nil {
		eztools.LogErrFatal(err)
	}
	defer db.Close()

	initVarsFromDb(db)
	// log file
	if len(paramL) > 0 {
		stat, err := os.Stat(paramL)
		if err == nil {
			if stat.Size() > 1024*1024 {
				switch eztools.PromptStr("Remove " + paramL + " because it is too big?([Enter]=y)") {
				case "y", "Y", "Yes", "YES", "yes", "":
					os.Remove(paramL) //TODO: backup before removal?
				}
			}
		}
		file, err := os.OpenFile(paramL, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err == nil {
			defer file.Close()
			_ = eztools.InitLogger(file)
		} else {
			eztools.ShowStrln("Failed to open log file")
		}
	}

	var serverGot bool
	upch := make(chan bool, 2)
	svch := make(chan string, 1)
	go db.AppUpgrade(tblDef, "Google", ver, &svch, upch)

	var readonlyStr string
	if !readonly {
		readonlyStr = "update"
	} else {
		readonlyStr = "list"
	}
	choices := []string{"quit", //0
		readonlyStr + " projects' statuses", //1
		readonlyStr + " requirements",       //2
		"get resource or list trasfers",     //3
		"list/get files on/from server"}     //4
	/*if !readonly {
		choices = append(choices, "get resource") //3
	}*/
	eztools.ShowStrln("checking for server...")

	serverGot = <-upch
	if serverGot {
		server = <-svch
	}
	cadb := make(chan string, 1)
	defer func() {
		cadb <- "STOP"
	}()
	go updatePrjBG(db, cadb)

EXIT:
	for {
		ch := make(chan string)
		// to make it fast for upReq, listReq for all actions
		go listReq(db, "", ch)
		// try to check phones' statuses without intervention
		c, _ := eztools.ChooseStrings(choices)
		switch c {
		case 0, eztools.InvalidID:
			break EXIT
		case 1:
			upPrj(db, readonly)
		case 2:
			upReq(db, ch, readonly)
		case 3:
			getRsrc(db)
		case 4:
			getFile(db)
		default:
			eztools.ShowStrln("impossible choice: " + strconv.Itoa(c))
		}
		for i := 0; i < 30; i++ {
			eztools.ShowStr("-")
		}
		eztools.ShowStrln("")
	}

	if serverGot {
		eztools.ShowStrln("waiting for update check to end...")
		<-upch
	}
}
