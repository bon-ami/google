module gitlab.com/bon-ami/google

go 1.15

require (
	gitee.com/bon-ami/eztools/v3 v3.2.1
	github.com/go-sql-driver/mysql v1.5.0
)
