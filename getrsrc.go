package main

import (
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"gitee.com/bon-ami/eztools/v3"
	_ "github.com/go-sql-driver/mysql"
)

func getFile(db *eztools.Dbs) {
	getInTran(db, false)
	fromN2 := eztools.PromptStr("Which dir to list on server or which file to get?([Enter]=root)")
	if len(fromN2) > 0 {
		toStr := eztools.PromptStr("Which dir to store the file?")
		if len(toStr) > 0 {
			fromN2 += "," + toStr
		}
	} else {
		url, err := db.GetPairStr(tblDef, "GoogleSvrFolder")
		if err != nil {
			eztools.LogErrPrintWtInfo("Database connection failure!", err)
			return
		}
		fromN2 = url + string(os.PathSeparator)
	}
	procNetNList(db, fromN2, "", "")
	getInTran(db, true)
}

func getRsrc(db *eztools.Dbs) {
	getInTran(db, false)
	//get product, android & tool
	mp, err := choosePairOrAdd(db, false, true, tblProduct, tblAndroid, tblTool)
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}

	//get tool
	tool, err := db.GetPairStr(tblTool, mp[tblTool])
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}

	//get bit
	sel := []string{fldBit}
	res, err := db.Search(tblProdfo,
		fldProduct+"="+mp[tblProduct]+" AND "+
			fldAndroid+"="+mp[tblAndroid], sel, "")
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}
	if len(res) != 1 {
		eztools.LogPrint("For product " + mp[tblProduct] +
			" and android " + mp[tblAndroid] + ", " +
			strconv.Itoa(len(res)) + " results found")
		return
	}
	bit, err := db.GetPairStr(tblBit, res[0][0])
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}

	android, err := db.GetPairStr(tblAndroid, mp[tblAndroid])
	if err != nil {
		eztools.LogErrPrint(err)
		return
	}
	procNetNList(db, tool, bit, android)
	getInTran(db, true)
}

func procNetNList(db *eztools.Dbs, tool, bit, android string) {
	eztools.ShowStr("Please wait patiently for result... ")
	rep, err := procNet(db, tool, bit, android)
	if err != nil || rep == nil {
		eztools.LogErrPrint(err)
		return
	}
	defer rep.Body.Close()
	switch rep.StatusCode {
	case 200:
		eztools.ShowStrln("Requested successfully.")
	case 400:
		eztools.ShowStrln("Wrong request!")
	case 502:
		eztools.ShowStrln("Server path not found!")
	case 503:
		eztools.ShowStrln("Wrong server path config!")
	default:
		eztools.ShowStrln("Please check with Allen...")
		eztools.Log(strconv.Itoa(rep.StatusCode) + " got from server!")
	}
	body, err := ioutil.ReadAll(rep.Body)
	if err == nil && len(body) > 0 {
		eztools.ShowStrln(string(body))
	} else {
		eztools.ShowStrln("<none>")
	}
}

func procNet(db *eztools.Dbs, tool, bit, android string) (rep *http.Response, err error) {
	url, err := db.GetPairStr(tblDef, "GoogleRes")
	if err != nil {
		return
	}
	req, err := http.NewRequest("GET", server+url, nil)
	if err != nil {
		return
	}
	q := req.URL.Query()
	if len(tool) > 0 {
		q.Add("tool", tool)
	}
	if len(bit) > 0 {
		q.Add("bit", bit)
	}
	if len(android) > 0 {
		q.Add("android", android)
	}
	req.URL.RawQuery = q.Encode()
	rep, err = http.DefaultClient.Do(req)
	return
}

func getInTran(db *eztools.Dbs, hint bool) {
	if hint {
		eztools.ShowStrln("Please note following list may not be accurate, since more downloads may be discovered while server is processing. Try to get resource again to get a refreshed list and check transfer status.")
	}
	procNetNList(db, "", "", "")
}
